import sys
import requests
import argparse
import config


class SonarrInstance:
    def __init__(self):
        self.shows = self.get_shows()
        self.tags = self.get_tags()

    # Get a list of shows from Sonarr
    def get_shows(self):
        return self.contact_sonarr('getting shows', 'GET', '/series')

    # Get a list of tags from Sonarr
    def get_tags(self):
        return self.contact_sonarr('getting tags', 'GET', '/tag')

    def create_tag(self, tag):
        return self.contact_sonarr('creating tag ' + tag, 'POST', '/tag',
                                   {'label': tag})

    def get_show(self, show_id, show_title):
        return self.contact_sonarr('retrieving show ' + show_title, 'GET',
                                   '/series/' + str(show_id))

    def update_show(self, show_json, tag, show_title):
        return self.contact_sonarr('adding tag {} to show {}'
                .format(tag, show_title), 'PUT', '/series', show_json)

    def contact_sonarr(self, action, http_method, endpoint, data=None):
        url = config.SONARR_URL + '/api' + endpoint + '?apikey=' + \
                config.API_KEY
        error_string = 'occured while ' + action
        try:
            response = requests.request(http_method, url=url, json=data)
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            raise SystemExit('HTTP Error ' + error_string + ': {}' .format(e))
        except requests.exceptions.ConnectionError as e:
            raise SystemExit('Connection Error ' + error_string +
                             ': {}' .format(e))
        except requests.exceptions.RequestException as e:
            raise SystemExit('An error ' + error_string + ': {}' .format(e))
        try:
            return response.json()
        except Exception as e:
            print("""
            Sonarr returned 200 but an error occurred parsing the response.
            Was 'api' in the URL? Please check URL for errors: {}
            Error: {}""" .format(url, e))
            raise


# Verify that a given tag exists, and create it if needed.
# Return the tag's ID
def get_tag(sonarr, tag, dry_run=False, create_tag=False):
    for t in sonarr.tags:
        if t['label'] == tag:
            return t['id']
    else:
        if create_tag:
            if dry_run:
                print("Tag {} will be created" .format(tag))
            else:
                response = sonarr.create_tag(tag)
                print("Tag {} was created". format(response['label']))
                return response['id']
        else:
            print("Tag {} doesn't exist. Please pass -c or --create-tag to"
                    " create it" .format(tag))
            sys.exit(1)


# Receive tag details and a path
# Add the tag to any shows that matches the path
def add_tag(sonarr, tag_id, paths, tag_name, dry_run=False):
    show_counter = 0
    for show in sonarr.shows:
        if any(path in show['path'] for path in paths)\
                and tag_id not in show['tags']:
            if dry_run:
                print(show['title'] + ' will be updated with the tag \''
                      + tag_name + '\'')
            else:
                show_counter += 1
                show_json = sonarr.get_show(show['id'], show['title'])
                show_json['tags'].append(tag_id)
                sonarr.update_show(show_json, tag_name, show['title'])
                print("{} was updated with tag {}"
                      .format(show['title'], tag_name))
    print("{} shows were updated with tag {}" .format(show_counter, tag_name))


def parse_arguments():
    parser = argparse.ArgumentParser(
            description='Add a tag to shows based on given paths')
    parser.add_argument('tag',
            help='The tag that will be added to each show')
    parser.add_argument('path', nargs='+',
            help='The paths that will be checked, separated by a space.')
    parser.add_argument('-c', '--create-tag', dest='create_tag',
                        action='store_true',
                        help='Create a tag if it doesn\'t exist')
    parser.add_argument('--dry-run', dest='dry_run', action='store_true',
            help='Only print the actions that will be taken without adding\
            the tags')
    parser.set_defaults(dry_run=False, create_tag=False)
    return parser.parse_args()


def main():
    my_sonarr = SonarrInstance()
    args = parse_arguments()
    tag_id = get_tag(my_sonarr, args.tag, args.dry_run, args.create_tag)
    add_tag(my_sonarr, tag_id, args.path, args.tag, args.dry_run)


if __name__ == "__main__":
    main()
