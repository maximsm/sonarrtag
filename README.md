# Description

This script adds a tag to a TV Show in Sonarr based on given paths.
Tested only on **Sonarr V2** and **python3**.

**Example:**  
`python sonarrtag.py Comedy /media/tvshows/comedies/ /media/tvshows/standup`

# Requirments
- sys
- requests
- argparse


# Configuration file
The script requires a config.py to be present in the same folder.

### config.py Example:
**Both of the fields are required**
> SONARR_URL = 'http://127.0.0.1:8989'  
>API_KEY = '1234567890'


# Usage
```
sonarrtag.py [-h] [-c] [--dry-run] tag path [path ...]

Add a tag to shows based on given paths

positional arguments:
    tag               The tag that will be added to each show
    path              The paths that will be checked, separated by a space.

optional arguments:
    -h, --help        show this help message and exit
    -c, --create-tag  Create a tag if it doesn't exist
    --dry-run         Only print the actions that will be taken without adding
                      the tags
```
